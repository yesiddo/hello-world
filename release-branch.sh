set -ex
# SET THE FOLLOWING VARIABLES
# docker hub username
USERNAME=fbrousse
# image name
IMAGE=hello-world
echo $CI_COMMIT_REF_NAME > VERSION
echo "version: $version"
# run build
./build.sh
docker tag $CI_REGISTRY_IMAGE:latest $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
# push it
docker push $CI_REGISTRY_IMAGE:latest
docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME